﻿using System.Reflection;
using Cyron43.GtaIV.Common;

namespace VehicleDelivery
{
   internal static class ModIdentityProvider
   {
      internal static IModIdentity Identity
      {
         get
         {
            return new ModIdentity
                   {
                      AssemblyName = Assembly.GetExecutingAssembly().GetName().Name,
                      FullPath = CommonFunctions.FileRepositoryPath + "VehicleDeliveryConfig.xml",
                      Version = Assembly.GetExecutingAssembly().GetName().Version.Major
                   };
         }
      }
   }
}