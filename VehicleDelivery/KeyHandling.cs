﻿using Cyron43.GtaIV.Common;
using GTA;

namespace VehicleDelivery
{
   public class KeyHandling : Script
   {
      private readonly CommonObjects _commonObjects;
      private readonly Core _core;
      // ReSharper disable once UnusedMember.Global
      public KeyHandling()
      {
      }

      public KeyHandling(Core core)
      {
         _core = core;
         _commonObjects = CommonObjects.CreateOrGetThis();
      }

      internal void HandleKey(KeyEventArgs e)
      {
         AtCallDeliverer(e);
      }

      private void AtCallDeliverer(KeyEventArgs e)
      {
         if(!KeyHandlingCommons.KeyIs(
            KeyHandlingCommons.GetKeyContainer(
               _core.Configuration.Keys, "VehicleDelivery"), e))
            return;
         if(!_core.DelivererIsOnDuty)
            _core.CallDeliverer();
         else
         {
            _commonObjects.Dismissed = true;
            _core.ReleaseDeliverer("Vehicle delivery has been aborted.");
         }
      }
   }
}