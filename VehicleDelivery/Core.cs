﻿using Cyron43.GtaIV.Common;
using GTA;

namespace VehicleDelivery
{
   // ReSharper disable once ClassNeverInstantiated.Global
   public class Core : Script
   {
      private const int SPAWNING_RADIUS = 100;
      private readonly CommonObjects _commonObjects;
      private readonly KeyHandling _keyHandling;
      private readonly TickEventHandlers _tickEventHandlers;
      private bool _fail;
      private ErrorType _typeOfError;

      public Core()
      {
         ReadConfiguration();
         if(_typeOfError != ErrorType.None)
            return;
         _commonObjects = CommonObjects.CreateOrGetThis();
         Interval = _commonObjects.GlobalTickInterval;
         _keyHandling = new KeyHandling(this);
         _tickEventHandlers = new TickEventHandlers(this);
         KeyDown += OnKeyDown;
      }

      internal ConfigurationContainer Configuration { get; private set; }
      internal bool DelivererIsOnDuty { get; private set; }

      internal void CallDeliverer()
      {
         ReadConfiguration();
         _fail = false;
         _commonObjects.Dismissed = false;
         DelivererIsOnDuty = true;
         _commonObjects.Target = Player.Character.Position;
         CreateVehicle();
         if(_fail)
            return;
         MakeVehicleDrive();
         CommonFunctions.DisplayText("The vehicle deliverer is on his way to you.", 5000);
      }

      internal void ObjectCreationFailed()
      {
         _tickEventHandlers.DetachTickEventFromArrivalHandler();
         _fail = true;
         ReleaseDeliverer("~y~Vehicle could not be spawned. "
                          + "Please check the model name in the configuration file.~w~");
      }

      internal void ReleaseDeliverer(string reason)
      {
         _tickEventHandlers.DetachTickEventFromArrivalHandler();
         DelivererIsOnDuty = false;
         if(CommonFunctions.PedExists(Player.Character))
            Player.Character.Invincible = false;
         CommonFunctions.DisplayText(reason, 3000);
         if(CommonFunctions.VehicleExists(_commonObjects.Vehicle))
         {
            if(CommonFunctions.PedExists(_commonObjects.Deliverer))
            {
               _commonObjects.Deliverer.FreezePosition = false;
               _commonObjects.Deliverer.Task.AlwaysKeepTask = false;
               if(Dismiss(reason))
                  return;
               _commonObjects.Vehicle.HazardLightsOn = false;
               _commonObjects.Vehicle.FreezePosition = false;
               _commonObjects.Vehicle.MakeProofTo(false, false, false, false, false);
               _commonObjects.Vehicle.CanBeDamaged = true;
               _commonObjects.Vehicle.CanBeVisiblyDamaged = true;
               _commonObjects.Vehicle.EngineRunning = false;
               _commonObjects.Deliverer.Task.AlwaysKeepTask = false;
               _commonObjects.Deliverer.Task.LeaveVehicle(_commonObjects.Vehicle, CloseDoor: true);
            }
         }
         if(CommonFunctions.PedExists(_commonObjects.Deliverer))
         {
            _commonObjects.Deliverer.CanBeDraggedOutOfVehicle = true;
            _commonObjects.Deliverer.Invincible = false;
            _commonObjects.Deliverer.Task.WanderAround();
            _commonObjects.Deliverer.NoLongerNeeded();
         }
         BlipHandling.RemoveAllBlips();
         if(_commonObjects.Dismissed)
         {
            _tickEventHandlers.PointTickEventToCruiseIfDismissedHandler();
         }
      }

      internal void Slowdown()
      {
         if(!CommonFunctions.VehicleExists(_commonObjects.Vehicle)
            || !CommonFunctions.PedExists(_commonObjects.Deliverer))
         {
            _commonObjects.Dismissed = true;
            ReleaseDeliverer("Oops, the deliverer had an accident! Call him again.");
            return;
         }
         _commonObjects.Deliverer.Task
            .CruiseWithVehicle(_commonObjects.Vehicle, 0.0f, ObeyTrafficLaws: false);
         while(_commonObjects.Vehicle.Speed > 0f)
            Wait(_commonObjects.GlobalTickInterval);
         ReleaseDeliverer("Your vehicle as you ordered, Sir.");
      }

      private void CreateDeliverer()
      {
         _commonObjects.Deliverer = null;
         if(!CommonFunctions.VehicleExists(_commonObjects.Vehicle))
         {
            ObjectCreationFailed();
            return;
         }
         _commonObjects.Deliverer = _commonObjects.Vehicle.CreatePedOnSeat(VehicleSeat.Driver);
         if(_commonObjects.Deliverer == null || !_commonObjects.Deliverer.Exists())
         {
            ObjectCreationFailed();
            return;
         }
         _commonObjects.Deliverer.CanBeDraggedOutOfVehicle = false;
         _commonObjects.Deliverer.Invincible = true;
         _commonObjects.Deliverer.Task.AlwaysKeepTask = false; // weil es beim Einsteigen auf true gesetzt wird.
      }

      private Vector3 CreateSpawnPosition()
      {
         return World.GetGroundPosition(
            (Player.Character.Position
             + Game.CurrentCamera.Direction
             * SPAWNING_RADIUS),
            GroundType.Highest)
                + Vector3.WorldUp;
      }

      private void CreateVehicle()
      {
         //var spawnPosition = Player.Character.Position.Around(SPAWNING_RADIUS).ToGround();
         var spawnPosition = CreateSpawnPosition();
         spawnPosition = World.GetNextPositionOnStreet(spawnPosition);
         _commonObjects.Vehicle = World.CreateVehicle(Configuration.VehicleModel, spawnPosition);
         if(!CommonFunctions.VehicleExists(_commonObjects.Vehicle))
         {
            ObjectCreationFailed();
            return;
         }
         _commonObjects.Vehicle.isRequiredForMission = true;
         _commonObjects.Vehicle.MakeProofTo(true, true, true, true, true);
         _commonObjects.Vehicle.PlaceOnNextStreetProperly();
         _commonObjects.Vehicle.Dirtyness = 0.0f;
         _commonObjects.Vehicle.CanBeDamaged = false;
         _commonObjects.Vehicle.CanBeVisiblyDamaged = false;
         _commonObjects.Vehicle.FreezePosition = false;

         CreateDeliverer();
         if(!_fail)
            BlipHandling.SetVehicleBlip(_commonObjects.Vehicle);
      }

      private bool Dismiss(string reason)
      {
         if(reason != "Vehicle delivery has been aborted.")
            return false;
         //question: keep task und dann clear all immediately? O_o
         _commonObjects.Deliverer.Task.AlwaysKeepTask = true;
         //_commonObjects.Deliverer.Task.ClearAllImmediately();
         _tickEventHandlers.PointTickEventToCruiseIfDismissedHandler();
         return true;
      }

      private void MakeVehicleDrive()
      {
         _commonObjects.Vehicle.EngineRunning = true;
         _commonObjects.Deliverer.Task.DriveTo(
            _commonObjects.Target,
            CommonFunctions.CorrectedSpeed(Configuration.Speed),
            Configuration.ObeyTrafficLaws,
            AllowToDriveRoadsWrongWay: false);
         _tickEventHandlers.AttachTickEventToArrivalHandler();
      }

      private void OnKeyDown(object sender, KeyEventArgs e)
      {
         _keyHandling.HandleKey(e);
      }

      private void ReadConfiguration()
      {
         Configuration = ConfigurationProvider.CreateOrGetThisFor(ModIdentityProvider.Identity)
            .GetConfigurationFor<ConfigurationContainer>(ModIdentityProvider.Identity, out _typeOfError);
      }
   }
}