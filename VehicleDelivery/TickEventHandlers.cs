﻿using System;
using System.Diagnostics;
using Cyron43.GtaIV.Common;
using GTA;

namespace VehicleDelivery
{
   internal class TickEventHandlers : Script
   {
      private static readonly object SyncLock = new object();
      private readonly CommonObjects _commonObjects;
      private readonly Core _core;
      private readonly Stopwatch _stopwatch;
      private Vector3 _lastVehiclePosition;

      public TickEventHandlers()
      {
         //GTA.Script verlangt einen parameterlosen Constructor.
      }

      internal TickEventHandlers(Core core)
      {
         _core = core;
         _commonObjects = CommonObjects.CreateOrGetThis();
         _stopwatch = new Stopwatch();
         _core.Tick += OnTick;
      }

      internal event Action Tock = delegate { };

      internal void AttachTickEventToArrivalHandler()
      {
         Tock += AtArrival;
      }

      internal void DetachTickEventFromArrivalHandler()
      {
         Tock -= AtArrival;
      }

      internal void PointTickEventToCruiseIfDismissedHandler()
      {
         Tock -= AtArrival;
         Tock += CruiseIfDismissed;
      }

      private void AtArrival()
      {
         lock(SyncLock)
         {
            if(!CommonFunctions.VehicleExists(_commonObjects.Vehicle))
            {
               _core.ObjectCreationFailed();
               return;
            }
            if(_commonObjects.Vehicle.Position.DistanceTo2D(_commonObjects.Target) > 10
               && VehicleHasMovedWithinTimeout())
               return;
            if(_commonObjects.Vehicle.Position.DistanceTo2D(_commonObjects.Target) > 10
               && !VehicleHasMovedWithinTimeout())
               TeleportVehicleToDestination();
            Player.Character.Invincible = true; //ensures player don't die when deliverer runs him over.
            _core.Slowdown();
            ResetMoveQuery();
         }
      }

      private void CruiseIfDismissed()
      {
         lock(SyncLock)
         {
            if(CommonFunctions.PedExists(_commonObjects.Deliverer)
               && CommonFunctions.VehicleExists(_commonObjects.Vehicle))
            {
               _commonObjects.Deliverer.Task.CruiseWithVehicle(_commonObjects.Vehicle,
                  CommonFunctions.CorrectedSpeed(25f), true);
            }
            PointTickEventToDeleteWhenOffScreenHandler();
         }
      }

      private void DeleteWhenOffScreen()
      {
         lock(SyncLock)
         {
            if(CommonFunctions.VehicleExists(_commonObjects.Vehicle)
               && !_commonObjects.Vehicle.isOnScreen)
            {
               if(CommonFunctions.PedExists(_commonObjects.Deliverer))
                  _commonObjects.Deliverer.Delete();
               _commonObjects.Vehicle.isRequiredForMission = false;
               _commonObjects.Vehicle.Delete();
               DetachTickEventFromDeleteWhenOffScreenHandler();
            }
            if(!CommonFunctions.PedExists(_commonObjects.Deliverer)
               && !CommonFunctions.VehicleExists(_commonObjects.Vehicle))
               DetachTickEventFromDeleteWhenOffScreenHandler();
            if(VehicleHasMovedWithinTimeout())
               return;
            if(CommonFunctions.PedExists(_commonObjects.Deliverer))
               _commonObjects.Deliverer.Delete();
            if(CommonFunctions.VehicleExists(_commonObjects.Vehicle))
               _commonObjects.Vehicle.Delete();
            DetachTickEventFromDeleteWhenOffScreenHandler();
         }
      }

      private void DetachTickEventFromDeleteWhenOffScreenHandler()
      {
         Tock -= DeleteWhenOffScreen;
      }

      private void OnTick(object sender, EventArgs e)
      {
         Tock();
      }

      private void PointTickEventToDeleteWhenOffScreenHandler()
      {
         Tock -= CruiseIfDismissed;
         Tock += DeleteWhenOffScreen;
      }

      private void ResetMoveQuery()
      {
         _stopwatch.Reset();
         if(CommonFunctions.VehicleExists(_commonObjects.Vehicle))
            _lastVehiclePosition = _commonObjects.Vehicle.Position;
      }

      private void TeleportVehicleToDestination()
      {
         ResetMoveQuery();
         _commonObjects.Vehicle.Position = _commonObjects.Target;
         _commonObjects.Vehicle.PlaceOnGroundProperly();
      }

      private bool VehicleHasMovedWithinTimeout()
      {
         if(_core.Configuration.ObeyTrafficLaws)
            return true; // not applicable if the undertaker has to obey the traffic laws.
         if(!CommonFunctions.VehicleExists(_commonObjects.Vehicle))
            return true;
         if(_commonObjects.Vehicle.Position.DistanceTo2D(_lastVehiclePosition) < 2
            && !_stopwatch.IsRunning)
            _stopwatch.Start();
         else if(_commonObjects.Vehicle.Position.DistanceTo2D(_lastVehiclePosition) >= 2)
            ResetMoveQuery();
         return _stopwatch.Elapsed.Seconds < 20;
      }
   }
}