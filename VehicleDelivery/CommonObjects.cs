﻿using System;
using GTA;

namespace VehicleDelivery
{
   internal class CommonObjects : Script
   {
      private static readonly object SyncLock = new object();
      private static CommonObjects myself;

      /// <summary>
      ///    Offenbar bekommt jede Klasse, die von Script erbt, eine eigene Interval-Instanz.
      /// </summary>
      internal int GlobalTickInterval = 250;

      /// <summary>
      ///    This constructor must not be used! CommonObjects is a singleton class but it needs a
      ///    public constructor because it inherits from GTA.Script.
      /// </summary>
      [Obsolete("Do NOT use this constructor!")]
      public CommonObjects()
      {
      }

      internal Ped Deliverer { get; set; }
      internal bool Dismissed { get; set; }
      internal Vector3 Target { get; set; }
      internal Vehicle Vehicle { get; set; }

      internal static CommonObjects CreateOrGetThis()
      {
         lock(SyncLock)
         {
            // ReSharper disable once CSharpWarnings::CS0618
            return myself ?? (myself = new CommonObjects());
         }
      }
   }
}