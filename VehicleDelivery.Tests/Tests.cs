﻿using System.Collections.Generic;
using System.Windows.Forms;
using Cyron43.GtaIV.Common;
using FluentAssertions;
using NUnit.Framework;

namespace VehicleDelivery.Tests
{
   [TestFixture]
   public class Tests
   {
      [Test]
      public void Reads_config()
      {
         ErrorType typeOfError;
         ConfigurationProvider.CreateOrGetThisFor(ModIdentityProvider.Identity)
            .GetConfigurationFor<ConfigurationContainer>(
               ModIdentityProvider.Identity,
               out typeOfError,
               supressInGameMessage: true);
         typeOfError.Should().Be(ErrorType.None);
      }

      [Test, Explicit]
      public void Saves_config()
      {
         var keyContainer = new KeyContainer
                            {
                               Alt = false,
                               Ctrl = false,
                               Key = Keys.B,
                               Name = "VehicleDelivery",
                               Shift = true
                            };

         var config = new ConfigurationContainer
                      {
                         Keys = new List<KeyContainer> {keyContainer},
                         ObeyTrafficLaws = true,
                         Speed = 20.0f,
                         VehicleModel = "Bus",
                         Version = ModIdentityProvider.Identity.Version
                      };
         ConfigurationProvider.SaveConfig(config, @"a:\VehicleDeliveryConfig.xml");
      }
   }
}