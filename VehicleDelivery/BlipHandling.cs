﻿using GTA;

namespace VehicleDelivery
{
   internal static class BlipHandling
   {
      private static Blip vehicleBlip;

      internal static void SetVehicleBlip(Vehicle vehicle)
      {
         vehicleBlip = vehicle.AttachBlip();
         vehicleBlip.Name = "Your vehicle delivery";
         vehicleBlip.Friendly = true;
         vehicleBlip.Color = BlipColor.Turquoise;
         vehicle.DoorLock = DoorLock.None;
      }

      internal static void RemoveAllBlips()
      {
         if(vehicleBlip != null && Game.Exists(vehicleBlip))
            vehicleBlip.Delete();
      }
   }
}