﻿using System.Collections.Generic;
using Cyron43.GtaIV.Common;

namespace VehicleDelivery
{
   public class ConfigurationContainer : IConfigurationContainer
   {
      public List<KeyContainer> Keys { get; set; }
      public bool ObeyTrafficLaws { get; set; }
      public float Speed { get; set; }
      public string VehicleModel { get; set; }
      public int Version { get; set; }
   }
}